terraform {
  backend "s3" {
    bucket = "igraylog-remote-state"
    region = "eu-central-1"
  }
}
