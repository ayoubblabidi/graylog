### ----- Variables definitions
variable environment {
  default = "develop"
}

variable aws_region {
  default = "eu-central-1"
}

variable default_tags {
  type = "map"

  default = {
    Owner      = "ANHO"
    Created_by = "Terraform"
  }
}

variable ssh_keypair_arn {
  default = "arn:xxx"
}

variable graylog_secret_arn {
  default = "arn:aws:xxx"
}

variable project_name {
  default = "iac-infrastructure-graylog"
}

variable vpc_id {
  default = ""
}

variable ec2_private_ips {
  default = [ "x.x.x.x" ]
}

variable ec2_subnet_ids {
  default = []
}

variable hostname {
  default = "hostname"
}

variable azs {
  default = ["eu-central-1a", "eu-central-1b"]
}

variable lb_subnets {
  default = ["subnet-xxx", "subnet-0xxx"]
}
variable instance_ami {
  default = "ami-0xxx"
}

variable instance_type {
  default = ""
}

variable root_volume_size {
  default = "120"
}

variable ec2_ingres_rules {
  default = []
}

variable ec2_egress_rules {
  default = []
}
